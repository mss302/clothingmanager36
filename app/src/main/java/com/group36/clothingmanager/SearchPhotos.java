package com.group36.clothingmanager;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.group36.clothingmanager.Models.Album;
import com.group36.clothingmanager.Models.Photo;
import com.group36.clothingmanager.Models.Tag;
import com.group36.clothingmanager.Models.UserModel;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.Serializable;


public class SearchPhotos extends AppCompatActivity {

    private TextView textviewSearch;
    private UserModel usermodel;
    private int i;
    private LinearLayout linlayout;
    private TextView textviewName;
    private TextView textviewTags;
    private TextView textviewCaption;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_photos);
        usermodel = (UserModel) getIntent().getExtras().getSerializable("usermodel");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        linlayout = (LinearLayout)findViewById(R.id.linlayout);
        textviewCaption = (TextView) findViewById(R.id.textviewCaption);
        textviewTags =(TextView)findViewById(R.id.textviewTags);
        textviewName = (TextView)findViewById(R.id.textviewName);
        textviewSearch = (TextView)findViewById(R.id.textviewSearch);

        Intent intent = getIntent();
        String tag1 = intent.getStringExtra("tag1");
        String tag2 = intent.getStringExtra("tag2");
        Photo temp = new Photo("temp","temp");
        usermodel.setcurrentphoto(temp);
        textviewSearch.setText(tag1 + " / " + tag2);
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
        for(Album a: usermodel.getcurrentuser().getAlbums()) {
            for (final Photo p : a.getPhotos()) {
                for(Tag t: p.getTags()) {
                    if(t.getKey().equals(tag1) && t.getValue().contains(tag2)) {

                        try {

                            ImageView im = new ImageView(this);

                            Bitmap bm = getBitmapFromUri(Uri.parse(p.getPath()));
                            Log.e("Uri String", p.getPath());
                            im.setId(i);
                            i++;
                            im.setImageBitmap(bm);

                            im.setPadding(2, 2, 2, 2);


                            im.setMinimumHeight(100);
                            im.setMinimumWidth(100);

                            usermodel.setcurrentphoto(p);
                            im.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Log.e("Image Clicked", p.toString());
                                    usermodel.setcurrentphoto(p);
                                    textviewName.setText("Name: " + usermodel.getcurrentPhoto().toString());
                                    textviewCaption.setText("Caption: " + usermodel.getcurrentPhoto().getCaption());
                                    textviewTags.setText("Tags: \n"+usermodel.getcurrentPhoto().getTags());
                                }
                            });

                            linlayout.addView(im);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }
        if(usermodel.getcurrentPhoto().equals(temp)){
            textviewName.setText("Name: none");
            textviewCaption.setText("Caption: none");
            textviewTags.setText("Tags: none");
        }else{
            textviewName.setText("Name: " + usermodel.getcurrentPhoto().toString());
            textviewCaption.setText("Caption: " + usermodel.getcurrentPhoto().getCaption());
            textviewTags.setText("Tags: \n"+usermodel.getcurrentPhoto().getTags());
        }




    }

    public void buttonViewPhoto(View view) throws IOException{
        Photo temp = new Photo("temp","temp");
        if(usermodel.getcurrentPhoto().equals(temp)){

        }else {
            Intent intent = new Intent(this, singlePhoto.class);
            intent.putExtra("usermodel", (Serializable) usermodel);
            startActivity(intent);
        }

    }
    private Bitmap getBitmapFromUri(Uri uri) throws IOException {

        return MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
    }

}
