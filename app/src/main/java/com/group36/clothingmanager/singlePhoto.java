package com.group36.clothingmanager;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.group36.clothingmanager.Models.Photo;
import com.group36.clothingmanager.Models.Tag;
import com.group36.clothingmanager.Models.UserModel;

import java.io.IOException;
import java.io.Serializable;

public class singlePhoto extends AppCompatActivity {

    private UserModel usermodel;
    private TextView textviewName;
    //private TextView textviewDate;
    private TextView textviewCaption;
    private TextView textviewTags;
    private TextView textviewError;
    private TextView textviewTagError;
    private EditText edittextRecaption;
    private EditText edittextTag;
    private EditText edittextMove;
    private Switch switchTag;
    private ImageView img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_view1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        usermodel = (UserModel) getIntent().getExtras().getSerializable("usermodel");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        intent.putExtra("usermodel",(Serializable)usermodel);
        textviewName = (TextView)findViewById(R.id.textviewName);
        //textviewDate = (TextView)findViewById(R.id.textviewDate);
        textviewCaption = (TextView)findViewById(R.id.textviewCaption);
        textviewTags = (TextView)findViewById(R.id.textviewTags);
        textviewError = (TextView)findViewById(R.id.textviewError);
        edittextRecaption = (EditText)findViewById(R.id.edittextRecaption);
        edittextTag = (EditText)findViewById(R.id.edittextTag);
        switchTag = (Switch)findViewById(R.id.switchTag);
        img = (ImageView)findViewById(R.id.img);
        textviewTagError = (TextView)findViewById(R.id.textviewTagError);
        switchTag.setVisibility(View.VISIBLE);
        edittextMove = (EditText)findViewById(R.id.edittextMove);

        textviewName.setText("Name: " + usermodel.getcurrentPhoto().toString());
        //textviewDate.setText("Date: " + usermodel.getcurrentPhoto().getModified());
        textviewCaption.setText("Caption: " + usermodel.getcurrentPhoto().getCaption());
        textviewTags.setText("" + usermodel.getcurrentPhoto().getTags());
        try {
            Bitmap ben = getBitmapFromUri(Uri.parse(usermodel.getcurrentPhoto().getPath()));
            img.setImageBitmap(ben);

        }catch(Exception e){
            Log.e("error", "e");
        }


    }
    private Bitmap getBitmapFromUri(Uri uri) throws IOException {

        return MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
    }
    public void recaptionPhoto(View view) throws IOException{

        textviewError.setVisibility(View.INVISIBLE);
        usermodel.getcurrentPhoto().setCaption(String.valueOf(edittextRecaption.getText()));
        textviewCaption.setText("Caption: " + String.valueOf(edittextRecaption.getText()));
        edittextRecaption.setText("");
        usermodel.closing(this.getApplicationContext());



    }
    public void deleteTag(View view) throws IOException{
        textviewTagError.setVisibility(View.INVISIBLE);
        Log.e("delete tag","has been clicked.");
        String tag1 = "";
        String tag2 = "";
        if(edittextTag.getText().equals("")){
            textviewTagError.setText("Tag cannot be empty.");
            textviewTagError.setVisibility(View.VISIBLE);
        }else {
            textviewTagError.setVisibility(View.INVISIBLE);
            if (switchTag.isChecked()) {
                tag1 = "place";
            } else {
                tag1 = "person";
            }
            tag2 = String.valueOf(edittextTag.getText());
            Tag temp = new Tag("","");
            boolean found = false;
            int count = 0;
            int i =0;
            for(Tag t : usermodel.getcurrentPhoto().getTags()){
                Log.e("loop","loop");
                if((t.getKey().equals(tag1))&&(t.getValue().equals(tag2))){
                    temp = t;
                    count++;

                }
                i++;
            }
            if(count==0){
                Log.e("didnt find","the tag");
                textviewTagError.setVisibility(View.VISIBLE);
            }
            while(count>0) {
                usermodel.getcurrentPhoto().getTags().remove(temp);
                count--;
            }

        }
        edittextTag.setText("");
        textviewTags.setText("" + usermodel.getcurrentPhoto().getTags());
        usermodel.closing(this.getApplicationContext());
    }
    public void addTag(View view) throws IOException{
        String tag1= "";
        String tag2= "";
        if(edittextTag.getText().equals("")){
            textviewError.setText("Tag cannot be empty.");
            textviewError.setVisibility(View.VISIBLE);
        }else{
            textviewError.setVisibility(View.INVISIBLE);
            if(switchTag.isChecked()){
                tag1 = "place";
            }else{
                tag1 = "person";
            }
            tag2 = String.valueOf(edittextTag.getText());
            usermodel.getcurrentPhoto().addTag(tag1,tag2);
            usermodel.closing(this.getApplicationContext());
            textviewTags.setText(usermodel.getcurrentPhoto().getTags() +"");
            edittextTag.setText("");
        }
        usermodel.closing(this.getApplicationContext());
    }
    public void prevPhoto(View view) throws IOException{
        if(usermodel.getcurrentAlbum().getSize()==1){

        }else{
            int count = 0;
            for(Photo p : usermodel.getcurrentAlbum().getPhotos()){
                if(p.toString().equals(usermodel.getcurrentPhoto().toString())){
                    break;
                }
                count++;
            }
            if(count-1 < 0){
                usermodel.setcurrentphoto(usermodel.getcurrentAlbum().getPhotos().get(usermodel.getcurrentAlbum().getSize()-1));
                Intent intent = new Intent(this,singlePhoto.class);
                intent.putExtra("usermodel",(Serializable)usermodel);
                usermodel.closing(this.getApplicationContext());
                startActivity(intent);
            }else{
                usermodel.setcurrentphoto(usermodel.getcurrentAlbum().getPhotos().get(count-1));
                Intent intent = new Intent(this,singlePhoto.class);
                intent.putExtra("usermodel",(Serializable)usermodel);
                usermodel.closing(this.getApplicationContext());
                startActivity(intent);
            }
        }
    }
    public void nextPhoto(View view) throws IOException{
        if(usermodel.getcurrentAlbum().getSize()==1){

        }else{
            int count = 0;
            for(Photo p : usermodel.getcurrentAlbum().getPhotos()){
                if(p.toString().equals(usermodel.getcurrentPhoto().toString())){
                    break;
                }
                count++;
            }
            if(count+1 >= usermodel.getcurrentAlbum().getSize()){
                usermodel.setcurrentphoto(usermodel.getcurrentAlbum().getPhotos().get(0));
                Intent intent = new Intent(this,singlePhoto.class);
                usermodel.closing(this.getApplicationContext());
                intent.putExtra("usermodel",(Serializable)usermodel);
                startActivity(intent);
            }else{
                usermodel.setcurrentphoto(usermodel.getcurrentAlbum().getPhotos().get(count+1));
                Intent intent = new Intent(this,singlePhoto.class);
                usermodel.closing(this.getApplicationContext());
                intent.putExtra("usermodel",(Serializable)usermodel);
                startActivity(intent);
            }
        }
    }
    public void moveToAlbum(View view) throws IOException{
        String albumName = String.valueOf(edittextMove.getText());
        if(edittextMove.getText().equals("")){
            textviewTagError.setText("Move album cant be empty");
            textviewTagError.setVisibility(View.VISIBLE);
        }else{
            for(int i =0; i<usermodel.getUser("currUser").getAlbums().size();i++){
                if(albumName.equals(usermodel.getUser("currUser").getAlbums().get(i).toString())){
                    Log.e("found album","moving photo...");
                    usermodel.getUser("currUser").getAlbums().get(i).addPhoto(usermodel.getcurrentPhoto());
                    usermodel.getcurrentAlbum().deletePhoto(usermodel.getcurrentPhoto().toString());
                    textviewTagError.setVisibility(View.INVISIBLE);
                    usermodel.closing(this.getApplicationContext());
                    Intent intent = new Intent(this, SplashPage.class);
                    intent.putExtra("usermodel", (Serializable) usermodel);
                    startActivity(intent);
                }else{
                    textviewTagError.setText("Could not find album.");
                    textviewTagError.setVisibility(View.VISIBLE);
                }
            }
        }

    }

}
