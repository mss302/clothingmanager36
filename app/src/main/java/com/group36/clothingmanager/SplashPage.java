package com.group36.clothingmanager;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.group36.clothingmanager.Models.Album;
import com.group36.clothingmanager.Models.User;
import com.group36.clothingmanager.Models.UserModel;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class SplashPage extends AppCompatActivity {

    private EditText albumName;
    private EditText editAlbum;
    private ListView listview;
    private TextView textError;
    private ArrayList<Album> list;
    private ArrayAdapter<Album> adapter;
    private TextView textviewAlbum;
    private Button buttonUpdate;
    private Button buttonEditAlbum;
    private Switch switchTag;
    private EditText editTextSearchTag;
    /*private TextView textviewRange;
    private TextView textviewOldest;*/
    private TextView textviewCount;
    UserModel um;


    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_page);

        try{
            um = new UserModel(this.getApplicationContext());
            if(!(um.isUser("currUser"))){
                um.createUser("currUser");
            }
            um.setcurrent(um.getUser("currUser"));
        }catch(Exception e){
            System.out.println("error in um"+e);
        }


        if(um!=null){
            list =  um.getcurrentuser().getAlbums();
        }
        else
            list = new ArrayList<Album>();

        adapter = new ArrayAdapter<Album>(this, android.R.layout.simple_list_item_1,list);
        listview = (ListView)findViewById(R.id.listViewAlbum);
        listview.setAdapter(adapter);
        textviewAlbum = (TextView)findViewById(R.id.textviewAlbum);
        textError = (TextView)findViewById(R.id.textError);
        editAlbum = (EditText)findViewById(R.id.editTextEditAlbum);
        buttonUpdate = (Button)findViewById(R.id.buttonUpdate);
        buttonEditAlbum = (Button)findViewById(R.id.buttonEditAlbum);
        switchTag = (Switch)findViewById(R.id.switchTag);
        editTextSearchTag = (EditText)findViewById(R.id.editTextSearchTag);
        textviewCount = (TextView)findViewById(R.id.textviewCount);
        /*textviewRange = (TextView)findViewById(R.id.textviewRange);
        textviewOldest = (TextView)findViewById(R.id.textviewOldest);*/

        if(um.getcurrentuser().getAlbums().size()==0){
            textviewCount.setText("Count: none");
            /*textviewRange.setText("Range: none");
            textviewOldest.setText("Oldest: none");*/
            textviewAlbum.setText("Album Name: none");
        }else{
            um.setcurrentalbum(um.getcurrentuser().getAlbums().get(0));
            textviewCount.setText("Count: " + um.getcurrentAlbum().getSize());
            /*textviewRange.setText("Range: " + um.getcurrentAlbum().rangeOfDates());
            textviewOldest.setText("Oldest: " + um.getcurrentAlbum().oldestPhoto());*/
            textviewAlbum.setText("Album Name: " + um.getcurrentAlbum().toString());

        }


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if(getCurrentFocus()!=null){
            InputMethodManager ben = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            ben.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                if(um.getcurrentuser().getAlbums().size()==0){
                    textviewAlbum.setText("Album Name: " + "none");
                }else{
                    textviewAlbum.setText("Album Name: " + um.getcurrentuser().getAlbums().get(position).toString());
                    um.setcurrentalbum( um.getcurrentuser().getAlbums().get(position));
                }

                if(um.getcurrentuser().getAlbums().size()==0){
                    textviewCount.setText("Count: none");
                    /*textviewRange.setText("Range: none");
                    textviewOldest.setText("Oldest: none");*/
                    textviewAlbum.setText("Album Name: none");
                }else{
                    textviewCount.setText("Count: " + um.getcurrentAlbum().getSize());
                    /*textviewRange.setText("Range: " + um.getcurrentAlbum().rangeOfDates());
                    textviewOldest.setText("Oldest: " + um.getcurrentAlbum().oldestPhoto());*/
                    textviewAlbum.setText("Album Name: " + um.getcurrentAlbum().toString());


                }
            }
        });
    }
    public void editSelected(View view) throws IOException{
        if(um.getcurrentAlbum().equals(null)) {
            textError.setVisibility(View.VISIBLE);
            textError.setText("You must select an album to edit.");
        }else {
            textError.setVisibility(View.INVISIBLE);
            buttonEditAlbum.setVisibility(View.INVISIBLE);
            buttonUpdate.setVisibility(View.VISIBLE);
            editAlbum.setText(um.getcurrentAlbum().toString());
        }
    }
    public void updateAlbum(View view) throws IOException{

        String albumEdit = String.valueOf(editAlbum.getText());
        if(albumEdit.equals("")){
            textError.setVisibility(View.VISIBLE);
            textError.setText("You must enter text to change the album name.");
        }else {
            boolean found = false;


            for(int i = 0;i<um.getcurrentuser().getAlbums().size();i++){
                if(um.getcurrentuser().getAlbums().get(i).toString().equals(albumEdit)){
                    found = true;
                    break;
                }else{

                }
            }
            if(found==false) {
                textError.setVisibility(View.INVISIBLE);
                um.getcurrentAlbum().updateName(albumEdit);
                um.closing(this.getApplicationContext());
                listview.setAdapter(adapter);
                textviewAlbum.setText("Album Name: " + albumEdit);
                editAlbum.setText("");
                buttonEditAlbum.setVisibility(View.VISIBLE);
                buttonUpdate.setVisibility(View.INVISIBLE);
                if (getCurrentFocus() != null) {
                    InputMethodManager ben = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    ben.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
            }else{
                textError.setVisibility(View.VISIBLE);
                textError.setText("Album name already exists!");
            }
        }
    }
    public void deleteAlbum(View view) throws IOException{
        if(um.getcurrentAlbum().equals(null)) {
            textError.setVisibility(View.VISIBLE);
            textError.setText("You must select an album to be deleted.");
        }else {
            textError.setVisibility(View.INVISIBLE);
            um.getcurrentuser().deleteAlbums(um.getcurrentAlbum().toString());
            um.closing(this.getApplicationContext());

            if(um.getcurrentuser().getAlbums().size()==0) {
                textviewAlbum.setText("Album Name: " + "none");
            }else{
                textviewAlbum.setText("Album Name: " + um.getcurrentuser().getAlbums().get(0).toString());
            }
            listview.setAdapter(adapter);
        }
    }
    public void addAlbum(View view)throws IOException{

        albumName = (EditText) findViewById(R.id.editTextAddAlbum);
        String albumNameString = String.valueOf(albumName.getText());


        if(albumNameString.equals("")) {
            textError.setText("Album must have a name!");
            textError.setVisibility(View.VISIBLE);

        }else{

            boolean found = false;
            Album alb = new Album(albumNameString);

            for(int i = 0;i<um.getcurrentuser().getAlbums().size();i++){
                if(um.getcurrentuser().getAlbums().get(i).toString().equals(albumNameString)){
                    found = true;
                    break;
                }else{

                }
            }

            if(found==false){

                um.getcurrentuser().addAlbums(alb);
                //adapter.add(alb);
                listview.setAdapter(adapter);
                um.closing(this.getApplicationContext());
                albumName.setText("");
                textError.setVisibility(View.INVISIBLE);
                textviewAlbum.setText("Album Name: " + albumNameString);


            }else {


                textError.setText("Album already exists!");
                textError.setVisibility(View.VISIBLE);
                albumName.setText("");
            }
            found = false;

        }

        if(getCurrentFocus()!=null){
            InputMethodManager ben = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            ben.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }

    }
    public void searchTag(View view) throws IOException{
        um.closing(this.getApplicationContext());
        String tag1 = "";
        String tag2 = String.valueOf(editTextSearchTag.getText());


        if(switchTag.isChecked()){
            tag1 = "place";
        }else{
            tag1 = "person";
        }

        if(tag2.equals("")){
            textError.setVisibility(View.VISIBLE);
            textError.setText("You must include a tag to search.");
        }else {
            Intent intent = new Intent(this, SearchPhotos.class);
            intent.putExtra("usermodel", (Serializable) um);
            intent.putExtra("tag1", tag1);
            intent.putExtra("tag2", tag2);
            startActivity(intent);
        }

    }
    public void viewAllPhotos(View view) throws IOException{
        um.closing(this.getApplicationContext());
        Intent intent = new Intent(this, allPhotos.class);
        intent.putExtra("user",(Serializable)um.getcurrentuser());
        intent.putExtra("album",(Serializable)um.getcurrentAlbum());
        intent.putExtra("usermodel", (Serializable)um);
        startActivity(intent);


    }

}



