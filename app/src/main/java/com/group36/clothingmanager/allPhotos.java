package com.group36.clothingmanager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.app.ListActivity;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;

import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.preference.DialogPreference;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.group36.clothingmanager.Models.Album;
import com.group36.clothingmanager.Models.Photo;
import com.group36.clothingmanager.Models.User;
import com.group36.clothingmanager.Models.UserModel;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;


public class allPhotos extends AppCompatActivity {


    private Intent intent;
    private Album alb;
    private User user;
    private UserModel usermodel;



    private EditText edittextRename;
    private ScrollView photoscrollView;
    private LinearLayout linlayout;
    private int i = 0;
    private ListView listViewPhotos;
    private ArrayList<Photo> list;
    private ArrayAdapter<Photo> adapter;
    private TextView textviewName;
    private TextView textviewCaption;
    //private TextView textviewDate;
    private TextView textviewTags;
    private TextView textviewError;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        usermodel = (UserModel) getIntent().getExtras().getSerializable("usermodel");
        setContentView(R.layout.activity_multiple_photo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        photoscrollView = (ScrollView) findViewById(R.id.photoScrollView);

        linlayout = (LinearLayout)findViewById(R.id.linlayout);
        Log.e("Break", "Point");

        
        textviewName = (TextView)findViewById(R.id.textviewName);
        textviewCaption = (TextView)findViewById(R.id.textviewCaption);
        //textviewDate = (TextView)findViewById(R.id.textviewDate);
        textviewTags = (TextView)findViewById(R.id.textviewTags);
        textviewError = (TextView)findViewById(R.id.textviewError);
        edittextRename = (EditText)findViewById(R.id.edittextRename);


        if(usermodel.getcurrentAlbum().getSize()==0){
            textviewName.setText("Name: " + "none");
            textviewCaption.setText("Caption: " + "none");
            //textviewDate.setText("Date: " + "none");
            textviewTags.setText("None \n " );
        }else{
            usermodel.setcurrentphoto(usermodel.getcurrentAlbum().getPhotos().get(0));
            textviewName.setText("Name: " + usermodel.getcurrentPhoto().toString());
            textviewCaption.setText("Caption: " + usermodel.getcurrentPhoto().getCaption());
            //textviewDate.setText("Date: " + usermodel.getcurrentPhoto().getModified());
            if(usermodel.getcurrentPhoto().getTags().size()==0){
                textviewTags.setText("None");
            }else {
                textviewTags.setText("" + usermodel.getcurrentPhoto().getTags());
            }
        }

        Intent intent = getIntent();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        String[] location = new String[] {"From Camera","From Device"};
        ArrayAdapter<String> adapt = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item ,location);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Image");
        builder.setAdapter(adapt, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which){
                if(which == 0){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


                    File file = new File(Environment.getExternalStorageDirectory(), "temp" + System.currentTimeMillis() + ".jpg");
                    imageCapture = Uri.fromFile(file);
                    try{
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCapture);
                        intent.putExtra("return data", true);
                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    dialog.cancel();
                }
                else{
                    Intent intent;
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
                        intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                    }else{
                        intent = new Intent(Intent.ACTION_GET_CONTENT);
                    }
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Pick"), PICK_FROM_FILE);

                }
            }
        });
        dialog = builder.create();
        Log.e("Break", "Point");

        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ) {

            // Should we show an explanation?


            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);

            // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
            // app-defined int constant



        }

        Log.e("Break", "Point");
        for(final Photo p: usermodel.getcurrentAlbum().getPhotos()){
            try{
                Log.e("adding a photo", "adding");
                ImageView im = new ImageView(this);

                Bitmap bm = getBitmapFromUri(Uri.parse(p.getPath()));
                Log.e("Uri String", p.getPath());
                im.setId(i);
                i++;
                im.setImageBitmap(bm);

                im.setPadding(2, 2, 2, 2);


                im.setMinimumHeight(100);
                im.setMinimumWidth(100);


                im.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        usermodel.setcurrentphoto(p);
                        Log.e("Image Clicked",p.toString());
                        textviewName.setText("Name: " + usermodel.getcurrentPhoto().toString());
                        textviewCaption.setText("Caption: " + usermodel.getcurrentPhoto().getCaption());
                        //textviewDate.setText("Date: " + usermodel.getcurrentPhoto().getModified());
                        if(usermodel.getcurrentPhoto().getTags().size()==0){
                            textviewTags.setText("None");
                        }else {
                            textviewTags.setText("" + usermodel.getcurrentPhoto().getTags());
                        }
                    }
                }


                );

                linlayout.addView(im);


            }catch (Exception e){
                e.printStackTrace();
            }

        }
        //imV = (ImageView) findViewById(R.id.img_show);


    }

    public void showSinglePhoto(View view){
        Intent intent = new Intent(this, singlePhoto.class);
        intent.putExtra("usermodel",(Serializable)usermodel);
        startActivity(intent);

    }
    private AlertDialog dialog;
    private Uri imageCapture;
    private ImageView imV;
    private int PICK_FROM_CAMERA = 1;
    private int PICK_FROM_FILE  = 2;

    public void addPhoto(View view){

        Log.e("Button", "add");
        dialog.show();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)  {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }
        Bitmap bm = null;
        String path = "";
        final Photo p;
        if (requestCode == PICK_FROM_FILE || requestCode == PICK_FROM_CAMERA) {
            Uri uri = data.getData();

            final int takeFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION;
            ContentResolver resolver = getContentResolver();
            resolver.takePersistableUriPermission(uri, takeFlags);
            try {
                bm = getBitmapFromUri(uri);
                p = new Photo("temp" + System.currentTimeMillis(), uri.toString());
                usermodel.getcurrentAlbum().addPhoto(p );
                Log.e("Uri String", uri.toString());
                Log.e("Saved Path", p.getPath());
                ImageView im = new ImageView(this);
                im.setId(i);
                im.setImageBitmap(bm);
                im.setMinimumHeight(100);
                im.setMinimumWidth(100);
                im.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        usermodel.setcurrentphoto(p);
                        Log.e("Image Clicked",p.toString());
                        textviewName.setText("Name: " + usermodel.getcurrentPhoto().toString());
                        textviewCaption.setText("Caption: " + usermodel.getcurrentPhoto().getCaption());
                        //textviewDate.setText("Date: " + usermodel.getcurrentPhoto().getModified());
                        if(usermodel.getcurrentPhoto().getTags().size()==0){
                            textviewTags.setText("None");
                        }else {
                            textviewTags.setText("" + usermodel.getcurrentPhoto().getTags());
                        }
                    }
                });
                linlayout.addView(im);
                usermodel.closing(this.getApplicationContext());
            }catch(Exception e){
                e.printStackTrace();

            }
        }
        else{

        }
        






    }
    @SuppressLint("NewApi")
    private Bitmap getBitmapFromUri(Uri uri) throws IOException {


        return MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void renamePhoto(View view) throws IOException{
        if(edittextRename.getText().equals("")){
            textviewError.setVisibility(View.VISIBLE);
            textviewError.setText("Must include a name.");
        }else{
            textviewError.setVisibility(View.INVISIBLE);
            String rename = String.valueOf(edittextRename.getText());

            for(Photo p:usermodel.getcurrentAlbum().getPhotos()){
                if(p.equals(usermodel.getcurrentPhoto())){
                    p.setName(rename);
                }
            }

            textviewName.setText(rename);
            edittextRename.setText("");
            usermodel.closing(this.getApplicationContext());
        }
    }

    public void deletePhoto(View view) throws IOException{
        int count=0;
        boolean found =false;
        if(usermodel.getcurrentPhoto().equals(null)){

        }else{
            for(Photo p:usermodel.getcurrentAlbum().getPhotos()){
                if(p.equals(usermodel.getcurrentPhoto())){
                    found = true;
                    break;
                }
                count++;
            }
            if(found) {
                usermodel.getcurrentAlbum().getPhotos().remove(count);
            }
            found = false;
        }
        usermodel.closing(this.getApplicationContext());
        Intent intent = new Intent(this,allPhotos.class);
        intent.putExtra("usermodel",(Serializable)usermodel);
        startActivity(intent);
        finish();

    }




}
