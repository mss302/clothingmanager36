package com.group36.clothingmanager.Models;

import android.content.Context;
import android.util.Log;
import java.io.*;
import java.util.ArrayList;


public class UserModel implements Serializable{

	private ArrayList<User> userlist;
	private User currentuser;
	private Album album;
	private Photo photo;

	
	
	public UserModel(Context context) throws ClassNotFoundException, IOException{
		userlist = new ArrayList<User>();
		deSer(context);
		Log.e("Finished", "deserializing");
		
	}
	public void setcurrent(User u){
		currentuser = u;
	}
	public void setcurrentalbum(Album a){
		album = a;
	}
	public void setcurrentphoto(Photo p) {photo = p;}
	public ArrayList<User> getUserList(){
		return userlist;
	}
	public void createUser(String username){
		User newUser = new User(username);
		userlist.add(newUser);
	}
	public void deleteUser(String username){
		
		for (User user : userlist) {
			if(user.username.equalsIgnoreCase(username)){
				userlist.remove(user);
				return;
			}
			
		}
	}
	public boolean isUser(String username){
		
		for (User user : userlist) {
			if(user.username.equalsIgnoreCase(username))
				return true;
			
		}
		return false;
		
	}
	public void closing(Context context) throws IOException{
		Ser(context);
		Log.e("Serialize", "Reached");
	}
	
	private void Ser(Context context1) throws IOException{
		
	         FileOutputStream fileOut = context1.openFileOutput("users.ser",Context.MODE_PRIVATE);
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         for(User user : userlist){
	        	out.writeObject(user);



	         }

	         out.writeObject(new User("ws9igsjlwoxop3u"));
	         out.close();
	         fileOut.close();
	}
	private void deSer(Context context1) throws FileNotFoundException, IOException, ClassNotFoundException{
		File f = context1.getFileStreamPath("users.ser");
		Log.e("Exists", f.exists()?"Yes":"No");
		if (f.exists()&&!f.isDirectory()){
			FileInputStream fileIn = context1.openFileInput("users.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			User e;
			while (((e = (User)in.readObject()) != null) && !e.username.equalsIgnoreCase("ws9igsjlwoxop3u")) {
				userlist.add(e);
				Log.e("Deserialize", e.username);

				
			}
			
			in.close();
		    fileIn.close();
			Log.e("Deserialize", "Reached");
			Log.e("First User", "currently unknown");
		}

	}
	public User getUser(String text) {
		for (User user : userlist) {
			if(user.username.equalsIgnoreCase(text))
				return user;
			
		}
		return null;
	}
	public User getcurrentuser() {

		return currentuser;
	}
	public Album getcurrentAlbum(){
		return album;
	}
	public Photo getcurrentPhoto() {return photo;}
	
	

}

