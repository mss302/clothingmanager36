package com.group36.clothingmanager.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7206687569296744864L;
	public User(String username2) {
		username = username2;
		
	}
	/**
	 * 
	 */
	public String username;
	public String password;
	private ArrayList<Album> albums= new ArrayList<Album>();
	public String toString(){
		return username;
	}
	/**
	 * @return the albums
	 */
	public ArrayList<Album> getAlbums() {
		return albums;
	}
	/**
	 * @param albums the albums to set
	 */
	public void addAlbums(Album album) {
		this.albums.add(album);
	}
	public void deleteAlbums(String name){
		for(Album album : albums){
			//albums.remove(album);
			if(album.toString().equals(name)){
				albums.remove(album);
				return;
			}
			
		}
		return;
	}
}
