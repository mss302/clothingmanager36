package com.group36.clothingmanager.Models;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Album implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Photo> photos = new ArrayList<Photo>();
	private String name; 
	
	public Album (String text){
		name = text;
	}
	public String toString(){
		return name;
	}
	public void updateName(String text){
		name = text;
	}
	public void addPhoto(Photo photo) {
		File thisphoto  = new File(photo.getPath());
		
		photo.setModified(new Date(thisphoto.lastModified()));
		this.photos.add(photo);
	}
	public void deletePhoto(String name){
		for(Photo photo : photos){
			//albums.remove(album);
			if(photo.toString().equals(name)){
				photos.remove(photo);
				return;
			}
			
		}
		return;
	}
	public ArrayList<Photo> getPhotos() {
		return photos;
	}
	public int getSize(){
		return photos.size();
	}
	public String oldestPhoto(){
		return endpoint(1);
	}
	
	public String endpoint(int top){
		
		if(photos.size()==0){
			return "no photos";
		}
		
		Photo oldest = photos.get(0);
		for(Photo photo: photos){
			if(photo.getModified().compareTo(oldest.getModified())<=top*-1){
				oldest = photo;
			}
		}
		return oldest.getModified().toString();
		
	}
	
	
	public String rangeOfDates(){
		String latest;
		String oldest;
		String range="";
		
		if(photos.size()==0){
			return "no photos";
		}
		
		oldest = endpoint(1);
		latest = endpoint(-1);
		
		
		range = oldest+","+latest;
		
		return range;
	}
}
