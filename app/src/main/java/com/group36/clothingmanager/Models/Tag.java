package com.group36.clothingmanager.Models;

import java.io.Serializable;

public class Tag implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String key;
    private String value;
    public Tag(String key1, String value1){
        key = key1;
        value = value1;
    }
    public String getKey(){ return key; }
    public String getValue(){ return value; }
    public void setKey(String key1){ key = key1; }
    public void setValue(String value1){ value = value1; }
    
    public String toString(){
    	return key + ":" + value;
    }
    
    
}
