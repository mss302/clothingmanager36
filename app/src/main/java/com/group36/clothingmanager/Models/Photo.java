package com.group36.clothingmanager.Models;


import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;

public class Photo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String path;
	private ArrayList<Tag> tags = new ArrayList<Tag>();
	private String name;
	private Date modified; 
	private String caption;
	
	public Photo(String text, String pathofphoto){
		name = text;
		path = pathofphoto;
		caption = text;
	}
	
	public String toString(){
		return name;
	}
	public String getPath(){
		return path;
	}
	public void addTag(String A, String B){
		tags.add(new Tag(A, B));
	}
	public ArrayList<Tag> getTags(){
		return tags;
	}
	public void setCaption(String Recaption){
		caption = Recaption;
	}
	public String getCaption(){
		return caption;
	}
	public void setName(String rename){
		name = rename;
	}
	/**
	 * @return the modified
	 */
	public Date getModified() {
		return modified;
	}

	/**
	 * @param modified the modified to set
	 */
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public void deleteTag(String tagname){
    	for(Tag tag1 : tags){
    		if(tag1.toString().equals(tagname)){
    			tags.remove(tag1);
    			break;
    		}
    	}
    }
	
}

	
	
